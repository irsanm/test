<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Bootstrap CSS -->
  <link href="dist/css/app.css" rel="stylesheet">

  <title>Hello, world!</title>
</head>

<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
      <a class="navbar-brand" href="<?= base_url(); ?>">Home</a>
      <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="#">Users</a>
          </li>

          <li class="nav-item">
            <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a>
          </li>
        </ul>
        <form class="d-flex">
          <input class="form-control me-2" type="search" placeholder="Search" aria-label="Search">
          <button class="btn btn-outline-success" type="submit">Search</button>
        </form>
      </div>
    </div>
  </nav> <!-- Content here -->


  <div class="container">
    <table class="table">
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Kode Transaksi</th>
          <th scope="col">Jenis Transaksi</th>
          <th scope="col">Bayar</th>
          <th scope="col">Kembali</th>
          <th scope="col">Diskon</th>
          <th scope="col">Total Transaksi</th>
          <th scope="col">Email</th>
          <th scope="col">Di Buat Oleh</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($cafe_jurnal as $i => $jurnal) : ?>
          <tr>
            <th scope="row"><?= $i + 1; ?></th>
            <td><?= $jurnal->kode_transaksi; ?></td>
            <td><?= $jurnal->jenis_transaksi; ?></td>
            <td><?= $jurnal->bayar; ?></td>
            <td><?= $jurnal->kembali; ?></td>
            <td><?= $jurnal->diskon; ?></td>
            <td><?= $jurnal->total_transaksi; ?></td>
            <td><?= $jurnal->email; ?></td>
            <td><?= $jurnal->created_by; ?></td>
            <td>@mdo</td>
          </tr>

        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
  <script src="dist/js/app.js"></script>
</body>

</html>