<?php
defined('BASEPATH') or exit('No direct script access allowed');

use chriskacerguis\RestServer\RestController;

class Cafe extends RestController
{

  function __construct()
  {
    // Construct the parent class
    parent::__construct();
  }

  public function jurnal_get()
  {
    $cafe_jurnal = $this->db->get("cafe_jurnal")->result_array();

    $id = $this->get('id');

    if ($id === null) {
      // Check if the users data store contains users
      if ($cafe_jurnal) {
        // Set the response and exit
        $this->response($cafe_jurnal, 200);
      } else {
        // Set the response and exit
        $this->response([
          'status' => false,
          'message' => 'No cafe_jurnal were found'
        ], 404);
      }
    } else {
      if (array_key_exists($id, $cafe_jurnal)) {
        $this->response($cafe_jurnal[$id], 200);
      } else {
        $this->response([
          'status' => false,
          'message' => 'No such user found'
        ], 404);
      }
    }
  }
}
